package ru.t1.dzelenin.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskCompleteByIndexResponse extends AbstractTaskResponse {

    public TaskCompleteByIndexResponse(@Nullable final TaskDTO task) {
        super(task);
    }

}
