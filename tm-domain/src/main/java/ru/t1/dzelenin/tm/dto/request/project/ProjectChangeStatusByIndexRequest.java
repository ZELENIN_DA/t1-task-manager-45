package ru.t1.dzelenin.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.request.AbstractIndexRequest;
import ru.t1.dzelenin.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectChangeStatusByIndexRequest extends AbstractIndexRequest {

    @Nullable
    private Status status;

    public ProjectChangeStatusByIndexRequest(
            @Nullable final String token,
            @Nullable final Integer index,
            @Nullable final Status status) {
        super(token, index);

        this.status = status;
    }

}

