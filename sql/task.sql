CREATE TABLE IF NOT EXISTS taskmanager.task
(
    name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    description character varying(500) COLLATE pg_catalog."default",
    status character varying(100) COLLATE pg_catalog."default",
    user_id character varying(100) COLLATE pg_catalog."default",
    created timestamp with time zone,
    id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    project_id character varying(50) COLLATE pg_catalog."default",
    date_begin timestamp without time zone,
    date_end timestamp without time zone
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS taskmanager.task
    OWNER to tm;