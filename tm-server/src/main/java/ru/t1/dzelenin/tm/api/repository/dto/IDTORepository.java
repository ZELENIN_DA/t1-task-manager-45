package ru.t1.dzelenin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.List;

public interface IDTORepository<M extends AbstractModelDTO> {

    void add(@NotNull M model);

    void set(@NotNull Collection<M> models);

    void update(@NotNull M model);

    @NotNull
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull String id);

    void remove(@NotNull M model);

    void removeById(@NotNull String id);

    void clear();

    long getCount();

    boolean existsById(@NotNull String id);

}